#include "my.h"

char *str_rot13(char *str)
{
    int i = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    while (str[i] != 0)
    {
        if (str[i] >= 65 && str[i] < 78)
            str[i] += 13;
        else if (str[i] >= 78 && str[i] <= 90)
            str[i] -= 13;
        else if (str[i] >= 97 && str[i] < 110)
            str[i] += 13;
        else if (str[i] >= 110 && str[i] <= 122)
            str[i] -= 13;
        i++;
    }
    return str;
}
