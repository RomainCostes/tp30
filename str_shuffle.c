#include "my.h"

void my_swap(char *a, char *b)
{
    char temp = *a;

    *a = *b;
    *b = temp;
}

char *str_shuffle(char *str)
{
    time_t t;
    int len = my_strlen(str) - 1;
    int i = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    srand((unsigned) time(&t));
    while (str[i] != '\0') {
        my_swap(&str[i], &str[rand() % len]);
        i++;
    }
    return str;
}
