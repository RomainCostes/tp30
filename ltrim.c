#include "my.h"

char *ltrim(char *str)
{
    int i = 0;
    int j = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    while (str[i] == ' ' || str[i] == '\t')
    {
        i++;
    }
    while (str[i] != '\0')
    {
        str[j] = str[i];
        i++;
        j++;
    }
    str[j] = 0;
    return str;
}
