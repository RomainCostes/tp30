#include "my.h"

char *str_repeat(char *str, int mul)
{
    int i = 0;
    int j = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    char *str2 = malloc(sizeof(char) * (my_strlen(str) * mul));

    if (str == NULL || str2 == NULL)
        return (NULL);
    while (mul > 0)
    {
        j = 0;
        while (str[j] != '\0')
        {
            str2[i] = str[j];
            i++;
            j++;
        }
        mul--;
    }
    str2[i] = 0;
    return (str2);
}
