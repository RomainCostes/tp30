#include "my.h"

char *trim(char *str)
{
    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    str = rtrim(str);
    str = ltrim(str);
    return str;
}

