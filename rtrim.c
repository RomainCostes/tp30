#include "my.h"

char *rtrim(char *str)
{
    int i = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    while (str[i] != '\0')
        i++;
    while (str[i] < 33)
        i--;
    str[i + 1] = 0;
    return str;
}
