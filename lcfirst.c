#include "my.h"

char *lcfirst(char *str)
{
    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    if (str[0] >= 'A' && str[0] <= 'Z')
        str[0] += 32;
    return str;
}
