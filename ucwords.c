#include "my.h"

char *ucwords(char *str)
{
    int i = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    if (str[0] >= 'a' && str[0] <= 'z')
        str[0] -= 32;
    while (str[i] != '\0')
    {
        if (str[i] == ' ')
        {
            i++;
            if (str[i] >= 'a' && str[i] <= 'z')
                str[i] -= 32;
        }
        i++;
    }
    return (str);
}
