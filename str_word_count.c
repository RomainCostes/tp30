#include "my.h"

int str_word_count(char *str)
{
    int i = 0;
    int count = 0;

    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    while (str[i] != '\0')
    {
        if (str[i] == ' ')
            count++;
        i++;
    }
    return count + 1;
}
