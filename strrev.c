#include "my.h"

char *strrev(char *str)
{
    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);

    int i = 0;
    int len = my_strlen(str);
    char *str2 = malloc(sizeof(char) * (len + 1));

    len--;
    while (len >= 0) {
        str2[i] = str[len];
        len--;
        i++;
    }
    str2[i] = 0;
    return str2;
}
