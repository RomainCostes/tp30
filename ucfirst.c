#include "my.h"

char *ucfirst(char *str)
{
    if (!str || str == NULL)
        return NULL;
    if (!str[0] || str[0] == 0)
        return (NULL);
    if (str[0] >= 'a' && str[0] <= 'z')
        str[0] -= 32;
    return (str);
}
