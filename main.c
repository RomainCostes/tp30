#include "my.h"

int main()
{
    char *test = strdup("TEST");
    printf("LCFIRST:\nbefore: %s -> ", test);
    printf("after: -%s-\n", lcfirst(test));
    test = strdup("test");
    printf("UCFIRST:\nbefore: %s -> ", test);
    printf("after: -%s-\n", ucfirst(test));
    test = strdup("     Hello wolrd !");
    printf("LTRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", ltrim(test));
    test = strdup("Hello wolrd !     ");
    printf("RTRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", rtrim(test));
    test = strdup("     Hello wolrd !     ");
    printf("TRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", trim(test));
    test = strdup("Hello wolrd !");
    printf("STR_PAD:\nbefore: %s -> ", test);
    printf("after: -%s-\n", str_pad(test, 20));
    test = strdup("Hello wolrd !     ");
    printf("RTRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", rtrim(test));
    test = strdup("Hello wolrd !     ");
    printf("RTRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", rtrim(test));
    test = strdup("Hello wolrd !     ");
    printf("RTRIM:\nbefore: %s -> ", test);
    printf("after: -%s-\n", rtrim(test));
    return 0;
}
