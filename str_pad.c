#include "my.h"

char *str_pad(char *str, int len)
{
    int i = 0;
    char *s2 = malloc(sizeof(char) * (len + 1));

    if (s2 == NULL)
        return NULL;
    if (my_strlen(str) >= len)
        return str;
    while (str[i] != '\0')
    {
        s2[i] = str[i];
        i++;
    }
    while (i < len)
    {
        s2[i] = ' ';
        i++;
    }
    s2[i] = 0;
    return s2;
}
