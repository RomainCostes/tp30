#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>

char *lcfirst(char *str);
char *ltrim(char *str);
char *rtrim(char *str);
char *str_pad(char *str, int len);
char *str_repeat(char *str, int mul);
char *str_rot13(char *str);
char *str_shuffle(char *str);
char *strrev(char *str);
char *strtolower(char *str);
char *strtoupper(char *str);
char *trim(char *str);
char *ucfirst(char *str);
char *ucwords(char *str);

void my_swap(char *a, char *b);

int my_strlen(char *str);
int str_word_count(char *str);
